<?php
function is_null_empty_or_notset($x) {
    return (null == $x || empty($x) || !isset($x));
}

function output_html($html_tag, $html_content, $self_closing=false) {
    if ($self_closing) return "<$html_tag value='$html_content'>";
    return "<$html_tag>$html_content</$html_tag>";
}

function calculate_reading_time($average_reading_time, $number_of_words) {
    if ($average_reading_time <= 0) return "n/a";
    return strval($number_of_words / $average_reading_time) . " mins";
}
?>
