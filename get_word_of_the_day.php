<?php
$url = "https://www.merriam-webster.com/word-of-the-day";
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $url);
$result_url = curl_exec($ch);
$str_parts = preg_split("/\r\n|\r|\n/", $result_url);
//echo '<pre>';print_r($str_parts);echo '</pre>';
$wotd = "none";
foreach ($str_parts as $line) {
    if (strpos($line, "<title>") !== false) {
        print_r(trim(explode(':', explode('|', $line)[0])[1]));
        $wotd = trim(explode(':', explode('|', $line)[0])[1]);
        break;
    }
}

$url = "https://api.dictionaryapi.dev/api/v2/entries/en/$wotd";
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $url);
$result_url = curl_exec($ch);
print_r(json_decode($result_url, true));
?>
